# dry database location.
db_path = r'dry.db'

# where to get resources from. can be one of: 'archive', 'refresh', 'disk'
# refresh - LittleBigRefresh server mirror, fastest option
# archive - the IA items, alternative in case refresh is down
# disk - zips on a local disk using a file size/offset cache
source = 'refresh'

# which directory to read from when using the disk source.
in_dir = r'.'

# which directory to output savedata to.
# e.g. r'C:\wherever\RPCS3\dev_hdd0\home\00000001\savedata' for RPCS3
# or r'E:\PS3\SAVEDATA' for a USB flash drive
# protip: in RPCS3 you don't need to restart the game for new saves to
# show in the import menu, so setting your output dir to the savedata
# folder like above makes playing levels more streamlined
out_dir = r'.'
