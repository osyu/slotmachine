# main script. see config.py to change options

import sqlite3
import tempfile
from pathlib import Path
from slotmachine.backup import write_backup
from slotmachine.const import SOURCES
from slotmachine.getdisk import gather_res_disk
from slotmachine.getweb import gather_res_web

DESCRIPTION = """Make backups for slots in the LBP level archive."""


def write_dry_backup(slot_id, lbp3, db, config):
    cur = db.cursor()

    cur.execute('SELECT * FROM slot WHERE id = ?', (slot_id,))
    if (row := cur.fetchone()) is None:
        raise LookupError(f"slot id {slot_id} not found")
    row = dict(row)

    row['name'] = row['name'] or "Unnamed Level"
    row['game'] = 2 if lbp3 else row['game']
    print(f"{repr(row['name'])} by {row['npHandle']}")

    start_shas = set()

    for resref in (row['rootLevel'], row['icon']):
        if resref is not None and len(resref) == 20:
            start_shas.add(resref)

    entries = {}

    if config.source == 'disk':
        gather_res_disk(start_shas, entries,
            Path(config.in_dir))
    elif config.source in SOURCES:
        gather_res_web(start_shas, entries,
            config.source)
    else:
        raise ValueError(f"invalid source {repr(config.source)}")

    for k, v in tuple(entries.items()):
        if v is None:
            print(f"warn: {k.hex()} missing")
            entries.pop(k)

    if row['rootLevel'] not in entries:
        raise Exception("root level resource missing")

    rlss = entries[row['rootLevel']][0]
    out_path = Path(config.out_dir)

    temp_dir = tempfile.TemporaryDirectory()
    temp_path = Path(temp_dir.name)

    try:
        write_backup(slot_id, row, rlss,
            entries, out_path, temp_path)
    finally:
        temp_dir.cleanup()


if __name__ == '__main__':
    import argparse
    import config

    parser = argparse.ArgumentParser(description=DESCRIPTION,
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('id', type=int,
        help="slot id")
    parser.add_argument('-3', '--lbp3', action='store_true',
        help="force lbp3 backup for 1/2 levels")

    args = parser.parse_args()

    db_path = Path(config.db_path)

    if not db_path.is_file():
        raise FileNotFoundError(
            f"database {repr(str(db_path))} not found")

    db = sqlite3.connect(db_path)
    db.row_factory = sqlite3.Row

    write_dry_backup(args.id, args.lbp3, db, config)
