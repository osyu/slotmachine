# functions for writing a PARAM.SFO (system file)

import builtins
from io import BytesIO

# IMPORTANT: these are LITTLE ENDIAN!
def u16(v): return v.to_bytes(2, 'little')
def u32(v): return v.to_bytes(4, 'little')
def utf8(v): return v.encode('utf-8') + b'\0'


def utf8_trunc(value, size):
    data = value.encode('utf-8')[:size - 1]
    return (data.decode('utf-8', 'ignore')
        .encode('utf-8')) + b'\0'


def write_sfo(fname, params):
    with (
        BytesIO() as f_index,
        BytesIO() as f_keys,
        BytesIO() as f_data
    ):
        for p in params:
            key = utf8(p[0])

            match type(p[1]):
                case builtins.bytes:
                    data = p[1]
                    dtype = 0x0004
                    size = max_size = len(p[1])
                case builtins.str:
                    dtype = 0x0204
                    max_size = p[2]
                    # limit/pad to max size
                    data = utf8_trunc(p[1], max_size)
                    size = len(data)
                    data += bytes(max_size - len(data))
                case builtins.int:
                    data = u32(p[1])
                    dtype = 0x0404
                    size = max_size = 4

            f_index.write(u16(f_keys.tell())) # key offset
            f_index.write(u16(dtype))
            f_index.write(u32(size))
            f_index.write(u32(max_size))
            f_index.write(u32(f_data.tell())) # data offset

            f_keys.write(key)
            f_data.write(data)

        sfo_index = bytes(f_index.getbuffer())
        sfo_keys = bytes(f_keys.getbuffer())
        sfo_data = bytes(f_data.getbuffer())

        # pad to 4 byte boundary
        sfo_keys += bytes(4 - (len(sfo_keys) % 4))

    with open(fname, 'wb') as f:
        f.write(b'\x00PSF') # magic
        f.write(u32(0x0101)) # version
        f.write(u32(20 + len(sfo_index))) # keys offset
        f.write(u32(20 + len(sfo_index)
            + len(sfo_keys))) # data offset
        f.write(u32(len(params))) # param count

        f.write(sfo_index)
        f.write(sfo_keys)
        f.write(sfo_data)
