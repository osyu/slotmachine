# function for writing a far level backup with accompanying
# files to make it importable on a normal PS3

import re
import shutil
import xxtea
from array import array
from hashlib import sha1
from sys import byteorder
from .const import TITLE_IDS, XXTEA_KEY
from .icon import write_icon
from .lbp import write_sltb, write_far
from .pfd import write_pfd
from .sfo import write_sfo

RE_LFILTER = re.compile(r'\W+|[^\x00-\x7F]+')
RE_FARNAME = re.compile(r'^[0-9]+$')
SPLIT_SIZE = 0x240000


def write_backup(slot_id, row, rlss, entries, out_path, temp_path):
    print(f"dataver: {rlss.dv}, brid: {rlss.bi}, brver: {rlss.bv}")

    savedata_dir = (
        TITLE_IDS[row['game']]
        + ('ADVLBP3AAZ' if row['isAdventurePlanet'] else 'LEVEL')
        + '%X' % slot_id
        + RE_LFILTER.sub('', row['name'].upper())
    )[:30]
    print(f"savedata dir: {savedata_dir}")

    print("building slotlist")
    sltb = write_sltb(rlss, row)
    root_hash = sha1(sltb).digest()
    entries[root_hash] = (rlss, sltb)

    ent_items = sorted(entries.items(), key=lambda x: x[0])

    print(f"building far backup with {len(entries)} resources")
    far = write_far(rlss, ent_items, root_hash)

    split_count = (len(far) // SPLIT_SIZE) + 1

    # the tea-ing here belongs in lbp but I super don't care
    print(f"writing {split_count} fragments")
    magic = None
    for i in range(split_count):
        cur = i * SPLIT_SIZE
        data = far[cur:cur + SPLIT_SIZE]

        if i == split_count - 1:
            magic = data[-4:]
            data = data[:-4]

        arr = array('I', data)
        if byteorder == 'little':
            arr.byteswap()

        arr = array('I', xxtea.encrypt(
            arr.tobytes(), XXTEA_KEY, padding=False))
        if byteorder == 'little':
            arr.byteswap()

        with open(temp_path / str(i), 'wb') as f:
            f.write(arr.tobytes())
            if magic is not None:
                f.write(magic)

    print("writing param.sfo")
    write_sfo(temp_path / 'PARAM.SFO', (
        ('CATEGORY', 'SD', 4),
        ('DETAIL', row['description'] or '', 1024),
        ('PARAMS', bytes(1024)),
        ('SAVEDATA_DIRECTORY', savedata_dir, 64),
        ('SUB_TITLE', f"{row['name']} by {row['npHandle']}", 128),
        ('TITLE', f"dry LBP{row['game'] + 1} backup", 128)
    ))

    pfd_ver = 3 if row['game'] < 2 else 4
    print(f"writing param.pfd v{pfd_ver}")
    write_pfd(temp_path / 'PARAM.SFO',
        temp_path / 'PARAM.PFD', pfd_ver)

    print("writing icon")
    write_icon(entries.get(row['icon']),
        row['game'], temp_path / 'ICON0.PNG')

    out_path = out_path / savedata_dir

    print(f"outputting to {str(out_path)}")

    if out_path.exists():
        for child in out_path.iterdir():
            if RE_FARNAME.match(child.name):
                child.unlink()

    shutil.copytree(temp_path, out_path,
        dirs_exist_ok=True)
