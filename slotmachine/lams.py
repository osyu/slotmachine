# LAMS stuff

def tag_to_lams(s):
    v0 = 0
    v1 = 0xc8509800

    for i in range(32, 0, -1):
        c = 0x20
        if (i - 1) < len(s):
            c = ord(s[i - 1])
        v0 = v0 * 0x1b + c

    if len(s) > 32:
        v1 = 0
        for i in range(64, 32, -1):
            c = 0x20
            if (i - 1) < len(s):
                c = ord(s[i - 1])
            v1 = v1 * 0x1b + c

    return (v0 + v1 * 0xdeadbeef) & 0xffffffff
