# functions for gathering resources from zip files
# on disk using the zip cache

from hashlib import sha1
from io import BytesIO
from .cache import sha_to_path, sha_to_idx
from .const import CACHE_NAME, HASHSTRUCT_SIZE, OFF_HASHES
from .lbp import parse_resource


def get_res_disk(sha, entries, in_path, f_cache):
    if sha in entries:
        return

    idx = sha_to_idx(f_cache, sha)
    if idx is None:
        print('!', end='', flush=True)
        entries[sha] = None
        return

    f_cache.seek(OFF_HASHES + (idx * HASHSTRUCT_SIZE) + 20)

    size = int.from_bytes(f_cache.read(4), 'little')
    offset = int.from_bytes(f_cache.read(8), 'little')

    with open(in_path / sha_to_path(sha.hex()), 'rb') as f:
        f.seek(offset)
        data = f.read(size)

    assert sha1(data).digest() == sha
    print('.', end='', flush=True)

    with BytesIO(data) as f:
        ss, deps = parse_resource(f, len(data))

    entries[sha] = (ss, data)

    for h in deps:
        get_res_disk(h, entries, in_path, f_cache)


def gather_res_disk(shas, entries, in_path):
    cache_path = in_path / CACHE_NAME

    if not cache_path.is_file():
        raise FileNotFoundError(
            f"cache {repr(str(cache_path))} not found")

    print('gathering resources', end='', flush=True)

    with open(cache_path, 'rb') as f:
        for sha in shas:
            get_res_disk(sha, entries, in_path, f)

    print()
