# functions for gathering resources from web

import asyncio
import aiohttp
from hashlib import sha1
from io import BytesIO
from aiolimiter import AsyncLimiter
from .const import REQ_PER_SEC, SOURCES
from .lbp import parse_resource


async def get_res_web(
    sha, entries, session, limiter,
    rurl_func, nf_status, req_sem, rlim_ev
):
    if sha not in entries:
        entries[sha] = None
    else:
        return

    async with req_sem:
        while True:
            await limiter.acquire(
                REQ_PER_SEC if rlim_ev.is_set() else 1)

            async with session.get(rurl_func(sha.hex())) as r:
                if r.status == 200:
                    data = await r.read()
                elif r.status == nf_status:
                    data = None
                else:
                    rlim_ev.set()
                    continue

                rlim_ev.clear()
                break

    if data is None:
        print('!', end='', flush=True)
        return

    assert sha1(data).digest() == sha
    print('.', end='', flush=True)

    with BytesIO(data) as f:
        ss, deps = parse_resource(f, len(data))

    entries[sha] = (ss, data)

    futures = (get_res_web(h, entries, session, limiter,
        rurl_func, nf_status, req_sem, rlim_ev) for h in deps)

    await asyncio.gather(*futures)


async def gather_res_web_async(shas, entries, source):
    print("gathering resources", end='', flush=True)

    limiter = AsyncLimiter(REQ_PER_SEC, 1)
    rurl_func, nf_status, max_concurrent = SOURCES[source]

    req_sem = asyncio.Semaphore(value=max_concurrent)
    rlim_ev = asyncio.Event()

    async with aiohttp.ClientSession() as session:
        futures = (get_res_web(h, entries, session, limiter,
            rurl_func, nf_status, req_sem, rlim_ev) for h in shas)

        # any resemblance is purely coincidental
        await asyncio.gather(*futures)

    print()


def gather_res_web(shas, entries, rurl_func):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(gather_res_web_async(
        shas, entries, rurl_func))
    loop.close()
