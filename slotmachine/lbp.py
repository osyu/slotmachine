# various hastily written lbp serialization functions,
# just enough to get it writing slotlists and far backups

# this should all be replaced with calls to a proper lbp
# python module in the (probably distant) future

# credit goes to aidan for most of the know-how:
# https://github.com/ennuo/toolkit

import hmac
import zlib
from dataclasses import dataclass
from hashlib import sha1
from io import BytesIO
from struct import pack
from .const import LABEL_TRANS_KEYS, HASHINATE_KEY

def ifbe(v): return int.from_bytes(v, 'big') # int-from-big-endian
def a3v(r, i): return (r >> 0x10) > i # after-lbp3-version
def round_up(x, l): return (x + l - 1) // l * l
def seek_pad(f, l): return f.seek(round_up(f.tell(), l))


@dataclass
class SerializeState:
    dv: int = 0 # data ver
    bi: bytes = b'\0\0' # branch id
    bv: int = 0 # branch ver
    f: BytesIO = None # file-like
    cm: bool = True # compress


def uleb128(ss, v):
    while v >= 0x80:
        ss.f.write(bytes(((v | 0x80) & 0xff,)))
        v >>= 7
    ss.f.write(bytes((v & 0xff,)))

def write_u8(ss, v):
    ss.f.write(pack('B', v))

def write_float(ss, v):
    ss.f.write(pack('>f', v))

def write_u32(ss, v):
    if ss.cm:
        uleb128(ss, v)
    else:
        ss.f.write(pack('>I', v))

def write_s32(ss, v):
    if ss.cm:
        uleb128(ss, (v & 0x7fffffff) << 1 ^ (v >> 0x1f))
    else:
        ss.f.write(pack('>i', v))

def write_str(ss, v):
    write_s32(ss, len(v))
    ss.f.write(v.encode('ascii'))

def write_wstr(ss, v):
    if v is None:
        write_s32(ss, 0)
        return
    write_s32(ss, len(v))
    ss.f.write(v.encode('utf-16-be'))


def write_res_desc(ss, v):
    if v is None:
        write_u8(ss, 0)
    elif type(v) is bytes:
        if len(v) == 20:
            write_u8(ss, 1 if ss.dv >= 0x191 else 2)
            ss.f.write(v)
        else:
            write_u8(ss, 2 if ss.dv >= 0x191 else 1)
            write_u32(ss, ifbe(v))
    else:
        raise ValueError("unknown res dtype")


def write_online_id(ss, v):
    assert len(v) <= 16
    if ss.dv < 0x234:
        write_u32(ss, 16) # len prefix
    ss.f.write(v.encode('ascii').ljust(16, b'\0') + b'\0') # npHandle
    if ss.dv < 0x234:
        write_u32(ss, 3) # len prefix
    ss.f.write(bytes(3)) # dummy


def write_slot_id(ss, _type, _id):
    write_u32(ss, _type)
    write_u32(ss, _id)


def write_slotlist(ss, row):
    write_u32(ss, 1) # slots[] size
    write_slot_id(ss, 6, 0) # slots[0].id
    write_res_desc(ss, row['rootLevel'] if not row['isAdventurePlanet'] else None) # slots[0].root
    if a3v(ss.dv, 0x144):
        write_res_desc(ss, row['rootLevel'] if row['isAdventurePlanet'] else None) # slots[0].adventure
    write_res_desc(ss, row['icon']) # slots[0].icon
    for i in range(4): # slots[0].location
        write_float(ss, 0.0)
    write_online_id(ss, row['npHandle']) # slots[0].authorID
    if ss.dv >= 0x13b:
        write_wstr(ss, row['npHandle']) # slots[0].authorName
    if ss.dv >= 0x183:
        write_str(ss, '') # slots[0].translationTag
    write_wstr(ss, row['name']) # slots[0].title
    write_wstr(ss, row['description']) # slots[0].description
    write_slot_id(ss, 0, 0) # slots[0].primaryLinkLevel
    if ss.dv >= 0x134:
        write_slot_id(ss, 0, 0) # slots[0].group
    write_u8(ss, row['initiallyLocked']) # slots[0].initiallyLocked
    if ss.dv >= 0x238:
        write_u8(ss, row['shareable']) # slots[0].shareable
        write_u32(ss, row['background'] or 0) # slots[0].backgroundGUID
    if ss.dv >= 0x333:
        write_res_desc(ss, None) # slots[0].planetDecorations
    if ss.dv < 0x188:
        write_u8(ss, 0) # unknown
    if ss.dv >= 0x1df:
        match row['leveltype']: # slots[0].developerLevelType
            case None:
                write_u32(ss, 0) # MAIN_PATH
            case 'versus':
                write_u32(ss, 6) # VERSUS
            case 'cutscene':
                write_u32(ss, 7) # CUTSCENE
            case _:
                raise ValueError("unknown leveltype")
    else:
        write_u8(ss, 0) # SideMission
    if ss.dv > 0x1ad and ss.dv < 0x1b9:
        write_u8(ss, 0) # unknown
    if ss.dv > 0x1b8 and ss.dv < 0x36c:
        write_u32(ss, 0) # gameProgressionState
    if ss.dv >= 0x33c:
        labels = []
        if row['authorLabels'] is not None:
            albli = ifbe(row['authorLabels'])
            for i in range(len(LABEL_TRANS_KEYS)):
                if (albli & (1 << i)):
                    labels.append(LABEL_TRANS_KEYS[i])
        write_u32(ss, len(labels))
        for i in range(len(labels)):
            write_u32(ss, labels[i])
            write_u32(ss, i)
    if ss.dv >= 0x2ea: # collectabubblesRequired
        write_u32(ss, 3)
        for i in range(3):
            write_res_desc(ss, None) # plan
            write_u32(ss, 0) # count
    if ss.dv >= 0x2f4: # collectabubblesContained
        write_u32(ss, 0)
    if ss.dv >= 0x352:
        write_u8(ss, row['isSubLevel']) # isSubLevel
    if ss.dv >= 0x3d0:
        write_u8(ss, row['minPlayers'] or 1) # minPlayers
        write_u8(ss, row['maxPlayers'] or 4) # maxPlayers
    if a3v(ss.dv, 0x215):
        write_u8(ss, 0) # enforceMinMaxPlayers
    if ss.dv >= 0x3d0:
        write_u8(ss, 0) # moveRecommended
    if ss.dv >= 0x3e9:
        write_u8(ss, 0) # crossCompatible
    if ss.dv >= 0x3d1:
        write_u8(ss, 1) # showOnPlanet
    if ss.dv >= 0x3d2:
        write_u8(ss, 0) # livesOverride
    if a3v(ss.dv, 0x12):
        write_u8(ss, 0) # gameMode
    if a3v(ss.dv, 0xd2):
        write_u8(ss, 0) # isGameKit
    if a3v(ss.dv, 0x11b):
        write_wstr(ss, '') # entranceName
        write_slot_id(ss, 0, 0) # originalSlotID
    if a3v(ss.dv, 0x153):
        write_u8(ss, 1) # customBadgeSize
    if a3v(ss.dv, 0x192):
        write_str(ss, '') # localPath
    if a3v(ss.dv, 0x206):
        write_str(ss, '') # thumbPath
    if ss.dv >= 0x3b6:
        write_u8(ss, 1) # fromProductionBuild


def write_sltb(ss, row):
    with BytesIO() as fsltb:
        with BytesIO() as fsl:
            ss.f = fsl
            write_slotlist(ss, row)
            ss.f = fsltb
            cmpd = zlib.compress(fsl.getbuffer(), level=9)
            compress = len(cmpd) < len(fsl.getbuffer())
            ss.f.write(b'SLTb')
            ss.f.write(ss.dv.to_bytes(4, 'big'))
            if ss.dv >= 0x109:
                ss.f.write(bytes(4)) # dep table offset, we'll get to this
            if ss.dv >= 0x271:
                ss.f.write(ss.bi)
                ss.f.write(ss.bv.to_bytes(2, 'big'))
            if ss.cm:
                ss.f.write(b'\7') # all compression
            if ss.dv >= 0x189:
                ss.f.write(bytes((compress,)))
            if compress or ss.dv < 0x189:
                if not compress:
                    ss.f.write(bytes(4))
                    ss.f.write(fsl.getbuffer())
                else:
                    # this is lazy but there's no way a slotlist
                    # will ever be 64 KiB so I don't really care
                    ss.f.write(b'\0\1\0\1') # unknown + chunk count
                    ss.f.write(len(cmpd).to_bytes(2, 'big'))
                    ss.f.write(len(fsl.getbuffer()).to_bytes(2, 'big'))
                    ss.f.write(cmpd)
            else:
                ss.f.write(fsl.getbuffer())
            if ss.dv >= 0x109:
                deptbl_offset = ss.f.tell()
                ss.f.seek(8)
                ss.f.write(deptbl_offset.to_bytes(4, 'big'))
                ss.f.seek(deptbl_offset)
                deps = (row['rootLevel'], row['icon'])
                ss.f.write(len(tuple(filter(None, deps))).to_bytes(4, 'big'))
                for i, dep in enumerate(deps):
                    if dep is None:
                        continue
                    if len(dep) == 20:
                        ss.f.write(b'\1') # hash
                        ss.f.write(dep)
                    else:
                        ss.f.write(b'\2') # guid
                        ss.f.write(ifbe(dep).to_bytes(4, 'big'))
                    if i == 0:
                        if row['isAdventurePlanet']:
                            ss.f.write(b'\0\0\0\x31') # ADC
                        else:
                            ss.f.write(b'\0\0\0\x09') # LVL
                    else:
                        ss.f.write(b'\0\0\0\1') # TEX
        return bytes(fsltb.getbuffer())


def write_far(ss, entries, root_hash):
    with BytesIO() as f:
        for entry in entries:
            f.write(entry[1][1])
        seek_pad(f, 4)
        f.write(ss.dv.to_bytes(4, 'big'))
        if ss.dv >= 0x271:
            f.write(ss.bi)
            f.write(ss.bv.to_bytes(2, 'big'))
        f.write(b'\0\0\0\1') # localUserId
        f.write(bytes(10 * 4)) # reserved1
        f.write(bytes(4)) # copied
        f.write(b'\0\0\0\x1d') # rtype
        f.write(bytes(3 * 4)) # reserved2
        f.write(root_hash)
        f.write(bytes(10 * 4)) # reserved3
        offset = 0
        for entry in entries:
            f.write(entry[0])
            f.write(offset.to_bytes(4, 'big'))
            f.write(len(entry[1][1]).to_bytes(4, 'big'))
            offset += len(entry[1][1])
        digest_offset = f.tell()
        f.write(bytes(20)) # digest, we'll get back to it
        f.write(len(entries).to_bytes(4, 'big'))
        f.write(b'FAR' + (b'4' if ss.dv >= 0x271 else b'3'))
        h = hmac.new(HASHINATE_KEY, digestmod=sha1)
        h.update(f.getbuffer())
        f.seek(digest_offset)
        f.write(h.digest())
        return bytes(f.getbuffer())


def parse_resource(f, size):
    ss = SerializeState()
    deps = set()

    f.seek(3) # seek to last char of magic
    if f.read(1) in b'be': # if res is binary...
        ss.dv = ifbe(f.read(4))
        if ss.dv < 0x109:
            return (ss, deps)

        deptbl_offset = ifbe(f.read(4))

        if ss.dv >= 0x271:
            ss.bi = f.read(2)
            ss.bv = ifbe(f.read(2))

        f.seek(deptbl_offset) # seek to dep table
        count = ifbe(f.read(4)) # read count

        cur = 0
        for _ in range(count):
            if cur >= size:
                deps.clear()
                break # dep table is malformed, abort

            flags = ifbe(f.read(1))
            if flags & 0x1: # if sha, add
                deps.add(f.read(20))
            elif flags & 0x2: # if guid, skip
                f.seek(4, 1)

            cur = f.seek(4, 1) # seek past rtype enum

    ss.cm = ss.dv >= 0x297 or (
        ss.dv == 0x272 and ss.bi == b'LD' and ss.bv >= 0x2)

    return (ss, deps)


def decompress_texture(f):
    if f.read(4) != b'TEX ':
        return None

    f.seek(2, 1) # unknown
    chunk_count = ifbe(f.read(2))

    chunks = []
    for _ in range(chunk_count):
        chunks.append((
            ifbe(f.read(2)), ifbe(f.read(2))))

    with BytesIO() as fo:
        for csize, dsize in chunks:
            if csize == dsize:
                fo.write(f.read(dsize))
            else:
                fo.write(zlib.decompress(
                    f.read(csize)))

        return bytes(fo.getbuffer())
