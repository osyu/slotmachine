# constants. no need to shout, no need to yell

from .lams import tag_to_lams

TITLE_IDS = ('BCES00141', 'BCES00850', 'BCES01663')

CACHE_NAME = 'dry.cache'
HASHSTRUCT_SIZE = (20 + 4 + 8)
OFF_HASHES = (2 ** 24) * 4

REQ_PER_SEC = 50
SOURCES = {
    'refresh': (lambda h: f'https://lbp.littlebigrefresh.com/api/v3/assets/{h}/download', 404, 32),
    'archive': (lambda h: f'https://archive.org/download/dry23r{h[0]}/dry{h[:2]}.zip/{h[:2]}/{h[2:4]}/{h}', 503, 64)
}

LABEL_TRANS_KEYS = (
    tag_to_lams('LABEL_SinglePlayer'),
    tag_to_lams('LABEL_RPG'),
    tag_to_lams('LABEL_Multiplayer'),
    tag_to_lams('LABEL_SINGLE_PLAYER'),
    tag_to_lams('LABEL_Musical'),
    tag_to_lams('LABEL_Artistic'),
    tag_to_lams('LABEL_Funny'),
    tag_to_lams('LABEL_Scary'),
    tag_to_lams('LABEL_Easy'),
    tag_to_lams('LABEL_Challenging'),
    tag_to_lams('LABEL_Long'),
    tag_to_lams('LABEL_Quick'),
    tag_to_lams('LABEL_Time_Trial'),
    tag_to_lams('LABEL_Seasonal'),
    tag_to_lams('LABEL_16_Bit'),
    tag_to_lams('LABEL_8_Bit'),
    tag_to_lams('LABEL_Homage'),
    tag_to_lams('LABEL_Technology'),
    tag_to_lams('LABEL_Pinball'),
    tag_to_lams('LABEL_Movie'),
    tag_to_lams('LABEL_Sticker_Gallery'),
    tag_to_lams('LABEL_Costume_Gallery'),
    tag_to_lams('LABEL_Music_Gallery'),
    tag_to_lams('LABEL_Prop_Hunt'),
    tag_to_lams('LABEL_Hide_And_Seek'),
    tag_to_lams('LABEL_Hangout'),
    tag_to_lams('LABEL_Driving'),
    tag_to_lams('LABEL_Defence'),
    tag_to_lams('LABEL_Party_Game'),
    tag_to_lams('LABEL_Mini_Game'),
    tag_to_lams('LABEL_Card_Game'),
    tag_to_lams('LABEL_Board_Game'),
    tag_to_lams('LABEL_Arcade_Game'),
    tag_to_lams('LABEL_Social'),
    tag_to_lams('LABEL_Sci_Fi'),
    tag_to_lams('LABEL_3rd_Person'),
    tag_to_lams('LABEL_1st_Person'),
    tag_to_lams('LABEL_CO_OP'),
    tag_to_lams('LABEL_TOP_DOWN'),
    tag_to_lams('LABEL_Retro'),
    tag_to_lams('LABEL_Tutorial'),
    tag_to_lams('LABEL_SurvivalChallenge'),
    tag_to_lams('LABEL_Strategy'),
    tag_to_lams('LABEL_Story'),
    tag_to_lams('LABEL_Sports'),
    tag_to_lams('LABEL_Shooter'),
    tag_to_lams('LABEL_Race'),
    tag_to_lams('LABEL_Platform'),
    tag_to_lams('LABEL_Puzzle'),
    tag_to_lams('LABEL_Gallery'),
    tag_to_lams('LABEL_Fighter'),
    tag_to_lams('LABEL_Competitive'),
    tag_to_lams('LABEL_Cinematic'),
    tag_to_lams('LABEL_FLOATY_FLUID_NAME'),
    tag_to_lams('LABEL_HOVERBOARD_NAME'),
    tag_to_lams('LABEL_SPRINGINATOR'),
    tag_to_lams('LABEL_SACKPOCKET'),
    tag_to_lams('LABEL_QUESTS'),
    tag_to_lams('LABEL_INTERACTIVE_STREAM'),
    tag_to_lams('LABEL_WALLJUMP'),
    tag_to_lams('LABEL_MEMORISER'),
    tag_to_lams('LABEL_HEROCAPE'),
    tag_to_lams('LABEL_ATTRACT_TWEAK'),
    tag_to_lams('LABEL_ATTRACT_GEL'),
    tag_to_lams('LABEL_Paint'),
    tag_to_lams('LABEL_Movinator'),
    tag_to_lams('LABEL_Brain_Crane'),
    tag_to_lams('LABEL_Water'),
    tag_to_lams('LABEL_Vehicles'),
    tag_to_lams('LABEL_Sackbots'),
    tag_to_lams('LABEL_PowerGlove'),
    tag_to_lams('LABEL_Paintinator'),
    tag_to_lams('LABEL_LowGravity'),
    tag_to_lams('LABEL_MagicBag'),
    tag_to_lams('LABEL_JumpPads'),
    tag_to_lams('LABEL_GrapplingHook'),
    tag_to_lams('LABEL_Glitch'),
    tag_to_lams('LABEL_Explosives'),
    tag_to_lams('LABEL_DirectControl'),
    tag_to_lams('LABEL_Collectables'),
    tag_to_lams('LABEL_CREATED_CHARACTERS'),
    tag_to_lams('LABEL_SACKBOY'),
    tag_to_lams('LABEL_SWOOP'),
    tag_to_lams('LABEL_TOGGLE'),
    tag_to_lams('LABEL_ODDSOCK')
)

HASHINATE_KEY = bytes.fromhex(
    '2afda3ca860219b3e68affcc82c76b8a'
    'fe0ad8135f60475bdf5d37bc571cb5e7'
    '9675d528a2fa90eddfa345b41ff91f25'
    'e742453b2bb53e16c958197be718c080')
XXTEA_KEY = bytes.fromhex(
    'bd0cb701d6079614d54df907a08cdb10')

SYSCON_MANAGER_KEY = bytes.fromhex(
    'd413b89663e1fe9f75143d3bb4565274')
KEYGEN_KEY = bytes.fromhex(
    '6b1acea246b745fd8f93763b920594cd53483b82')
SAVEGAME_PARAM_SFO_KEY = bytes.fromhex(
    '0c08000e090504040d010f000406020209060d03')
