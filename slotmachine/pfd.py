# function for writing a PARAM.PFD (protected file database),
# which in our case is just a grossly overcomplicated
# signature for the PARAM.SFO. other games use this file to
# sign and encrypt their savedata, but since LBP does its own
# encryption, its savedata PFDs only sign the SFO. this code
# writes the bare minimum PFD that the PS3 will accept

# for info on the format read the pfdtool src:
# https://github.com/bucanero/pfd_sfo_tools

from io import BytesIO
from Crypto.Cipher import AES
from Crypto.Hash import HMAC, SHA1
from .const import (
    SYSCON_MANAGER_KEY,
    KEYGEN_KEY,
    SAVEGAME_PARAM_SFO_KEY
)

def u64(v): return v.to_bytes(8, 'big')


def write_pfd(sfoname, fname, version):
    # nulled out per-PARAM.PFD keys for simplicity
    pf_header_iv = bytes(16) # for header encryption
    pf_key_orig = bytes(20) # hmac key for various stuff
    if version == 4:
        pf_key = HMAC.new(KEYGEN_KEY,
            pf_key_orig, SHA1).digest()
    else:
        pf_key = pf_key_orig

    pf_index_size = 1 # normally 57, we only need 1
    pf_entry_size = 1 # normally 114, we only need 1

    sfo_file_name = b'PARAM.SFO'.ljust(65, b'\0')
    with open(sfoname, 'rb') as f:
        sfo_file_hash = HMAC.new(SAVEGAME_PARAM_SFO_KEY,
            f.read(), SHA1).digest()
        sfo_file_size = f.tell()

    # the only protected file entry, for our PARAM.SFO
    with BytesIO() as f:
        f.write(u64(pf_index_size)) # next entry index (last one)
        f.write(sfo_file_name)
        f.write(bytes(7)) # padding
        f.write(bytes(64)) # file encryption key
        f.write(sfo_file_hash)
        # the hashes below and encryption key above
        # are just for encryption and copy protection;
        # we can null them out since the ps3 doesn't
        # check them in our case
        f.write(bytes(20)) # console id hash
        f.write(bytes(20)) # disc key hash
        f.write(bytes(20)) # account id hash
        f.write(bytes(40)) # reserved
        f.write(u64(sfo_file_size))
        pf_entries = bytes(f.getbuffer())

    # protected file index
    with BytesIO() as f:
        f.write(u64(pf_index_size))
        f.write(u64(pf_entry_size)) # reserved entries
        f.write(u64(pf_entry_size)) # used entries
        f.write(u64(0)) # PARAM.SFO entry index
        pf_index = bytes(f.getbuffer())

    # signature doesn't include next entry index or
    # the padding after file name
    pf_entries_trim = sfo_file_name + pf_entries[80:]

    # only one pf entry, so only one sig
    # in the sig table
    pf_entry_sig_table = HMAC.new(pf_key,
        pf_entries_trim, SHA1).digest()

    # signature for pf index
    pf_index_sig = HMAC.new(pf_key,
        pf_index, SHA1).digest()

    # signature for pf entry sig table
    pf_entry_sig_table_sig = HMAC.new(pf_key,
        pf_entry_sig_table, SHA1).digest()

    # header
    with BytesIO() as f:
        f.write(pf_entry_sig_table_sig)
        f.write(pf_index_sig)
        f.write(pf_key_orig)
        f.write(bytes(4)) # padding
        pf_header = bytes(f.getbuffer())

    # encrypt header
    pf_header = AES.new(SYSCON_MANAGER_KEY,
        AES.MODE_CBC, pf_header_iv).encrypt(pf_header)

    with open(fname, 'wb') as f:
        f.write(u64(0x50464442)) # 'PFDB' magic
        f.write(u64(version))

        f.write(pf_header_iv)
        f.write(pf_header)

        f.write(pf_index)
        f.write(pf_entries)
        f.write(pf_entry_sig_table)
