# functions for writing xmb icons

from io import BytesIO
from pathlib import Path
from PIL import Image, UnidentifiedImageError
from .lbp import decompress_texture

BADGE_POS = (80, 8)


def crop_square(im):
    width, height = im.size
    sqsize = min(im.size)

    return im.crop((
        (width - sqsize) // 2,
        (height - sqsize) // 2,
        (width + sqsize) // 2,
        (height + sqsize) // 2))


def write_icon(tex, game, out_path):
    asset_path = Path(__file__).parent / 'assets'

    im_base = Image.open(asset_path / f'icon{game}.png')
    mask = Image.open(asset_path / 'mask.png').convert('L')

    data = None
    if tex is not None:
        with BytesIO(tex[1]) as f:
            data = decompress_texture(f) or tex[1]

    try:
        im_icon = Image.open(BytesIO(data))
    except UnidentifiedImageError:
        im_icon = Image.open(asset_path / f'empty{game}.png')

    im_icon = im_icon.convert('RGBA')
    im_back = Image.new('RGBA', im_icon.size,
        (192, 192, 192, 255))
    im_icon = Image.alpha_composite(
        im_back, im_icon)

    im_icon = crop_square(im_icon)
    im_icon = im_icon.resize(mask.size,
        Image.Resampling.HAMMING)
    im_icon.putalpha(mask)

    im = Image.new('RGBA', im_base.size,
        im_base.getpixel((0, 0)))

    im.paste(im_icon, BADGE_POS, im_icon)
    im = Image.alpha_composite(im, im_base)

    im.save(out_path)
