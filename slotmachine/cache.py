# utility functions for dealing with the zip cache

from .const import HASHSTRUCT_SIZE, OFF_HASHES


def sha_to_path(sha):
    return f'dry23r{sha[0]}/dry{sha[:2]}.zip'


def sha_to_idx(f, sha):
    f.seek(int.from_bytes(sha[:3], 'big') * 4)

    first_idx = int.from_bytes(f.read(4), 'little')
    t_count = first_idx >> 26
    first_idx &= 0x3ffffff

    for i in range(t_count):
        f.seek(OFF_HASHES + ((first_idx + i) * HASHSTRUCT_SIZE))

        if f.read(20) == sha:
            return first_idx + i

    return None
