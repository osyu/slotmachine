# script to generate a zip cache

import zipfile
from io import BytesIO
from pathlib import Path
from slotmachine.cache import sha_to_path
from slotmachine.const import CACHE_NAME


def write_triptet(f, triptet, t_count, prev_idx):
    if t_count >= (1 << 6):
        raise ValueError("triptet count overflow")

    f.seek(int.from_bytes(triptet, 'big') * 4)
    f.write((prev_idx | (t_count << 26))
        .to_bytes(4, 'little'))


def generate_cache(in_path):
    triptet = None
    t_count = 0
    idx = 0
    prev_idx = 0

    with (
        BytesIO() as f_index,
        BytesIO() as f_table
    ):
        for i in range(256):
            zip_path = in_path / sha_to_path('%02x' % i)

            print(f"reading {zip_path}")

            with zipfile.ZipFile(zip_path, mode='r') as zf:
                for zi in zf.infolist():
                    sha = bytes.fromhex(zi.filename[6:])
                    size = zi.file_size
                    offset = zi.header_offset + 76

                    if sha[:3] != triptet:
                        if triptet is not None:
                            write_triptet(f_index, triptet,
                                t_count, prev_idx)
                            t_count = 0

                        triptet = sha[:3]
                        prev_idx = idx

                    f_table.write(sha)
                    f_table.write(size.to_bytes(4, 'little'))
                    f_table.write(offset.to_bytes(8, 'little'))

                    t_count += 1
                    idx += 1

        write_triptet(f_index, triptet, t_count, prev_idx)

        with open(in_path / CACHE_NAME, 'wb') as f:
            f.write(f_index.getbuffer())
            f.write(f_table.getbuffer())


if __name__ == '__main__':
    import config

    print("generating cache, this will take a while")

    in_path = Path(config.in_dir)

    generate_cache(in_path)
